---
layout: default
title: Acerca
permalink: /about/
---

# Que somos?

Un espacio de intercambio y producción colectiva, abierto, cuidado, horizontal y adhocrático para abordar, investigar y performar utilizando técnicas y herramientas de livecoding

abierto: en el sentido que toda participación, propuesta, idea, herramienta es bienvenida

de intercambio y producción: porque consideramos que el saber es una construcción social comunitaria, y pertenece a la comunidad, no al individuo (por lo tanto, el siguiente punto)

colectivo y abierto: cualquier persona puede formar parte del colectivo en tanto tenga ganas y pueda llevar adelante las ideas presentadas en el presente

adhocrático y horizontal: porque no hay jerarquía, no hay ni habrá dueñxs, ni directorxs, ni maestrxs. Rechazamos los yo-ismos, el egocentrismo y la pedagogía pedante

intercambio y producción: el objetivo final es poder investigar, aprender, llevar adelante propuestas artísiticas, performáticas, fechas, tocar y disfrutarnos
cuidado: porque no permitimos comentarios ofensivos o desvalorizantes de ninguna índole; relacionados con el/los géneros, la identidad, la orientación sexual, las capacidades diferentes, apariencia física, tamaño corporal, ni gustos musicales o usos de herramientas de software.

Mas data: [https://colectivo-de-livecoders.gitlab.io/](https://colectivo-de-livecoders.gitlab.io/)
