---
layout: post
categories: posts
title: "Turorial Tidal"
---

# Intento de descripción del funcionamiento de algunos Comandos en TidalCycles (y en otros lados también)

## Indice

1. [Algoritmo Euclideano](#1-algoritmo-euclideano)

2. [Mixer Ritchse/Lil' Data/ Kindhom / Otres?](#2-mixer-ritchselil-datakindhom-style)

3. [La Doble Booleana ft Martín Karadagian](#3-la-doble-booleana)

4. [Instalación de Nuevos Efectos en TidalCycles](#4-nuevos-efectos-de-superdirt---windows)


## 1. Algoritmo Euclideano

                    `d1 $ s "arpy(e,p,g)"`

El algoritmo euclideano (o euclidean en ingles) divide al ciclo en P (pasos) y ocupa E (eventos) en el. 
La cuenta que hace es dividir a E sobre P pero sin decimales así que agrupa "de la manera mas pareja posible", 
dando por resultados ritmos piolas(divertidos, slang de argentina). A su vez si ponemos valor a G (golpe) va a mover el comienzo de frase y por lo tanto cambiando el resultado sonoro. 
Esto es genial y se puede usar de muchas maneras.
```
d1 $ stack[
  s "arpy(3,8)",
  s "arpy(3,8,2)" # up "-2",
  s "arpy(3,8,6)" # up "-5",
  s "arpy(3?,8,4)" # up "-12",
  s "ht:1(5,8)",
  s "ht:2(5,8,3)",
  s "~"
]
```

Es interesante como se aglutinan el mismo sample con distintas alturas, o distintos samples que representan partes de una bateria (o como quieran usarlo)
`# up "valor"` mueve a nuestro sample dentro de una escala cromatica (osea en semitonos)
? le da 50% de posibilidades de sonar o no sonar al evento en cuestión, en este caso, puede ser que sean 3 E o menos, o ninguno.
Los patitos < > (o picos) nos van a dar un valor por ciclo, lo que nos puede servír en este caso para cambiár el lugar de G y 
modificar el ritmo de un ciclo al siguiente modificar P y tener menos o mas pasos o cambiar E y modificar cuantos eventos sonaran por ciclo.//

```
d2 $ stack[
  palindrome $ s "arpy(3,8)" # up "-12",
  s "[jvbass(3,8,2)]*2",
  s "[latibro(3,8,4)]/2" # up "7",
  s "~"
]
```
El efecto palindrome (palindromo, como el capicúa "arriba la birra" "anita lava la tina") genera ese efecto sobre el algoritmo euclideano (y en cualquier lista de samples/eventos)
dandonos el patron e invertido sobre p (osea nos va a dar vuelta el 3 3 2 que hacemos con (3,8) o el ritmo que tengamos, osea es interesante[1]). 
    >Atención, si no me equivoco, palindrome toca si o si tanto el originál como el capicúa osea que si usamos <> durara el doble de ciclos (puede no gustar?).

Poner el algoritmo por fuera de los corchetes [] acomodara la lista de samples sobre P tomando el orden de los samples como si fuera G, es decír que el primer
sample tendra los eventos desde el primer lugar, el segundo desde el segundo, y así. Si queremos ubicar algun sample en otro lugar de G podemos usar ~ como silencio.
```
d3 $ s "[arpy ~ ~ ht ~ ~ cp ~ ~](3,8)"
```
Podemos correr a su vez a todo el patron sobre G, poniendole un valor mayor a 0 a este.
  
Si usamos el algoritmo en un solo evento este puede ser duplicado o dividido por fuera de los corchetes, igual que cualquier patron de samples.
```
d4 $ stack[
  s "[bassfoo(3,8)]*2" # gain "0.9",
  s "[jvbass(5,8,<2 5>)]/2" # gain "1.3",
  s "~"
]
``` 

```
        original         capicúa
(3,8)   E + + E + + E +  + E + + E + + E
(3,8,1) + E + + E + + E  E + + E + + E +
(3,8,2) E + E + + E + +  + + E + + E + E
(3,8,3) + E + E + + E +  + E + + E + E +
(3,8,4) + + E + E + + E  E + + E + E + +
(3,8,5) E + + E + E + +  + + E + E + + E
(3,8,6) + E + + E + E +  + E + E + + E +
(3,8,7) + + E + + E + E  E + E + + E + +
```
La lista anteriór grafica el resultado del euclideano de 3 sobre 8, imaginense cambiar estos valores de manera aleatoria, por listas, o de multiples maneras.
Esto aquí presente se estudia mucho en general y es interesante el desafio de incorporarlo corporalmente[2], a demás de usarlo en nuestro código. 
También es interesante el resultado sonoro que nos da usar el mismo sample o sinte con distintas alturas, 
dandonos como resultado una __sensación__ de tambor o algo similar.

[1] buscar paper "euclidean rythm on algorithmic music" autor ¿?  

[2] https://www.youtube.com/watch?v=EZNAne1tpoQ Jorge Drexler sobre la milonga, la rima por décimas y el ritmo del 3 3 2. 
ir al minuto 01:42 

--------------------------

## 2. Mixer Ritchse/lil Data/kindhom style

para hacer esto tenemos que armar un bloque do, que como su nombre en ingles indica es un bloque que "hace" algo. recomiendo habilitar la identación (o como se llame) en atom (yo uso atom, mis ejemplos se basan en el SNS vim y emacs) entonces

0. habilitar identado
-esto nos va a servír porque el bloque do es muy quisquilloso con la manera en la que escribimos nuestro codigo así que las lineas nos van a guiar para que quede todo funcionando
-ir a (bueno se me tildo atom, mientras reinicio el editor sigo con el resto)

1. bloque do y la variable del mixer
vamos a crear un bloque do pegado al ¿margen? del editor
debajo de esto pondremos let mixer (o el nombre que le quieras poner a tu mixer) con la cantidad de "canales" entre corchetes
en este ejemplo lo vamos a hacer con dos canales nomas, lo que nos quedaria así:

do
let mixer = [0,0] (las comas son fundamentales) 0 es apagado (ya vamos a ver luego) y 1 sería prendido, si queremos poner mas canales simplemente ponemos, 0,0 por la cantidad de canales que querramos.

esto no hace nada 1ro porque al no terminar con una "acción" el bloque do tira error  [algune programadore podria explicar esto mejor que yo] y segundo porque no tiene ningún canal asignado

2. canales y __magia__
luego de armar el bloque do y la variable mixer 
vamos a poner nuestros dos patrones ritmicos en sus respectivos canales/orbits, me pierdo con tantas maneras de leer a cada cosa, sería lo siguiente

```
do
let mixer = [0,0]
d1 $ s "bd"
d2 $ s "~ cp"
```

esto tampoco va a funcionar porque nos falta la parte mas importante, que es conectar a nuestros d1 y d2 con la mixer de la siguiente manera
|* gain (mixer !! 0)
1   2     3    4  5

1. |* (este simbolo sirve para multiplicar el valor ya dado de algo (en este caso la ganancia) por el numero que pongamos aquí)
2. como vamos a dejar sonar o silenciar cada canal queremos modificar la ganancia de estos, 0 no suena 1 suena (incluso podemos subir y o bajar el volumen lo que querramos, es decír 0.2 0.8 1.2, [guarda con valores mas altos que 1.5]
3. mixer es el nombre de la variable que iniciamos mas arriba, si la llamaramos con otro nombre aquí iria ese otro nombre
4. no se que implica !! supongo que le habla al lugar n (n siendo el numero que esta en 5) 
5. aquí estaría el lugar que queremos asignarle en nuestra mixer, es indice 0 así que el primer numero es 0 y a partir de ahí sube.

ATENCIÓN

si queremos usar xfade o alguna transicion similar, esta la vamos a hacer por fuera del bloque do PERO luego tendriamos que silenciar el canal en la mixero comentarlo (es decír o borrarlo o que quede en gris (-- d1 $ s "bd") porque si no después cuando le demos control enter al bloque do (con el canal sin silenciar o sin comentar) este va a sobreescribir el xfade perdiendo la gracia del fade out.

¿que es una mixer?
una mixer es un dispositivo (hardware o software) en donde mezclamos (por eso mixer) los volumenes de nuestros instrumentos conectados al dispositivo.
cuando hacemos |* gain (mixer !! (numero del canal)) estamos conectando el o los instrumentos a la mixer general, permitiendonos prender o apagarlo (1 o 0)
o subir y bajar el volumen (valores intermedios 0.2, 0.4,0.8, etc(guarda con valores mayores a 1.5))
es igual, o similar a una consola fisica con faders, o a los faders en ableton,reaper y similar.

----------------------


## 3. La doble booleana

boole era une ¿informatique? que describió la logica del verdadero/falso que le da vida al sistema binario de 0 y 1, pensemos que todo pensamiento dual sigue en algun punto la logica de boole, shing/shang, dia/noche, ser/nada, dios/humanidad (a re filosofique)
en fin, estamos constantemente utilizando estas logicas en nuestros scripts concientes o no, PERO en general siempre es UNA opcion verdadero o falso, aquí les vengo a ofrecer para el codigo de le livecoder LA DOBLE BOOLEANA
esta se realizará sobre la mixer anteriór.

```
do
let mixer = [1,1]
let dobleB = "[1 0 1 0]"
let invertir x = x - 1
d1 $ gain dobleB # s "bd"
d2 $ gain (invertir <$> dobleB) # s "snare"
```

primero la booleana simple, en la variable "dobleB" ponemos un patron de 1 y 0 que nos va a prender y o apagar aquello que le asignemos la variable en cuestión
la variable "invertir" como su nombre indica, va a invertir este valor, así que aquello que sea 0 lo leera como uno y aquello que sea 1 lo leera como 0. esto nos va a permitir un lindo bombo caja en el ejemplo.

AHORA - porque doble booleana? bueno es simple, Tidal (o bueno aquellos lenguajes o dispositivos que tengan la siguiente opción) incluye al silencio (o no leer sample ninguno) con el simbolo de la "ñufla/cosito" ~ si ponemos eso no va a leer ni 0 ni 1 así que será silencio real para ambas opciones. dejandonos una base con la logica booleana PERO que incluye un tercer valor (ya ni las computadoras son binarias :P ) entonces podriamos hacer cosas como:
  
  
```  
do
let mixer = [1,1]
let dobleB = "[1 0 <[~ 1] 1 0> <0 ~ [1 0]>]"
let invertir x = x - 1
d1 $ gain dobleB # s "bd"
d2 $ gain (invertir <$> dobleB) # s "snare"
```

si se fijan en el nuevo ejemplo gracias a los patitos <> en un ciclo tendremos [~ 1] (silencio y bd (recordemos que bd es 1) en otro bd solo y en el ultimo snare.
en el otro <> tenemos snare, silencio total y [bd snare].

Algo interesante que tiene la doble booleana (que ya podria ser hasta la triple y transformarse en una maniobra digna de karadajian) es la posibilidad de poner la variable dobleB en la nota en vez de en la ganancia, lo que funcionaria igual, pero en vez de ser -prendido apagado- seria nota 0 nota 1, si esto lo utilizamos en carpetas con dos samples (yo tengo una carpeta con un hihat abierto y un hihat cerrado) nos va a dar un patron rítmico interesante que pase por los dos samples en cuestión.
sería algo así

```
do
let mixer = [1,1]
let dobleB = "[1 0 1 0]"
let invertir x = x - 1
d1 $ gain dobleB # s "bd"
d2 $ gain (invertir <$> dobleB) # s "snare"
d3 $ n dobleB # s "hh" |+ n "6"
(hago esto para que le sume 6 al valor default 0 de la nota "hh" suelta y suene el sample 6 y 7 de la carpeta que son dos sonidos simil hihat abierto-hihat cerrado, yo personalmente prefiero tener una carpeta con dos samples y listo, pero lo arme así para que cualquier persona con Dirt-Samples lo pueda probar)
```

-----------------------------------------

### 4. Nuevos Efectos de SuperDirt - WINDOWS

Actualizar SuperDirt yendo a la consola (cmd) buscar la carpeta de SuperDirt 
[en mi caso ..c: usuarios/ nombre de user/ AppData/Local/Supercollider/downloaded-quarks/SuperDirt ]
 ejecutar el comando `git checkout develop` deberia andar

### Código para SuperCollider [para que funcione]

https://github.com/musikinformatik/SuperDirt/blob/develop/library/default-effects-extra.scd

//instalar todo, compilar la clase de SC (languaje/compile class library), arrancar SuperDirt, correr el codigo de de aca arriba

Copiar este codigo en atom->Codigo para Atom [modo tutorial]

https://gist.github.com/jarmitage/c8fba2a94f7219e984df8772f96e30d0

### Doc google en plan tutorial

https://docs.google.com/document/d/1P-_DGKtdJKyVsjQKAjCWlLBlI8hA_5XKrGwErK6FNhw/edit
