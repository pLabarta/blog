---
layout: post
categories: posts
title: " 7 motivos por los que el live coding crecerá"
---

# 7 motivos por los que el live coding crecerá

#### Por [Federico Simonetta](https://www.techeconomy.it/2019/02/18/7-motivi-per-cui-live-coding-crescera/)- Traducción al español [Sofía Cascardo](https://www.instagram.com/umachinita/)


Del 14 al 17 de febrero se llevó a cabo una importante [Algorave](https://toplap.org/wearefifteen/): 84 horas de música sin interrupción, en la que, probablemente, fue la manifestación musical online más grande que haya habido. El 14 de febrero, de hecho, se cumplió el décimo quinto aniversario del nacimiento de TOPLAP, la comunidad online internacional de músiques de live coding, o artistas que improvisan música programando en tiempo real. Como dice un [post](https://toplap.org/toplap-15th-anniversary-stream-14-17th-february-2019/) publicado en el sitio de la comunidad, el 14 de febrero de 2004, menos de una decena de live coders se reunieron en Hamburgo para encontrarse y conocerse.
Personas provenientes de diferentes partes del mundo, pioneras en un modo de hacer música al que aún muchas se oponen, aunque la comunidad ahora cuente con  más de 2000 personas inscriptas en el chat colectivo de TOPLAP.

Un movimiento que se ha ampliado gradualmente, manteniendo como característica la idea de utilizar la computadora como un verdadero instrumento musical para ser explotado en performances improvisadas y en tiempo real. El segundo aspecto característico de TOPLAP está en los primeros dos puntos del [manifiesto](https://toplap.org/wiki/ManifestoDraft): apertura total en el conocimiento musical, de los instrumentos utilizados y del proceso creativo. Les livecoders de TOPLAP muestran la pantalla al público para que todes puedan ver como programan. Una forma de recrear la gestualidad de la performance musical, que varies expertes destacan como fundamental para el disfrute de la música. Esto no debe considerarse como ostentación de la técnica porque, como está escrito en el manifiesto, “no es necesario que el público entienda el código para apreciarlo, así como no hace falta saber tocar la guitarra para apreciar un solo de guitarra”.

Aunque la práctica de improvisar música con la computadora es bastante antigua y podemos encontrarla hasta en los años "80, el mérito de TOPLAP fue el de organizar la subcultura del livecoding y de crear las herramientas, aunque fueran rudimentarias, para que se comunicaran les artisas, llegando a una definición bastante precisa del movimiento.

El crecimiento repentino de TOPLAP, sobre todo en los últimos años, es un fenómeno que no debe subestimarse y que, de hecho, puede ofrecerle a les musiques una nueva oportunidad para relanzar el significado del “en vivo” en la era de los registros digitales, ya que nos acostumbraron a la música que produce obras inmutables en el tiempo, iguales, hoy como hace diez años.

### 7 motivos por los cuales el live coding crecerá 

El live coding seguirá evolucionando, creciendo, y será uno de los movimientos musicales más interesantes de este siglo. Mientras intento explicar el porqué, se puede escuchar de fondo cualquier video de les artistes que seleccioné dándole importancia a la variedad de estilos: [Alexandra Cárdenas – street code](https://www.youtube.com/watch?v=0Vh7xLZRI78), [Andrew Sorensen – jazz](https://www.youtube.com/watch?v=bq-260NUw5o), [Ganzfeld – experimental](https://www.youtube.com/watch?v=F-XPLeimmQY), [Shelly Knots – experimental](https://www.youtube.com/watch?v=zmtLDbWXNeI), [Benoît and the Mandelbrots – étnica/experimental](https://www.youtube.com/watch?v=jlx6KNo5Eok), [Sam Aaron – dance](https://www.youtube.com/watch?v=G1m0aX9Lpts)


### 1. Les Live Coders vienen de dos contraculturas y crearon una nueva

El live coding nace, resumidamente, del encuentro de dos subculturas: la de les hackers y la de la música electrónica, con constantes referencias a la cultura rave.
Aunque la comunidad está de acuerdo en considerar la cultura rave como solo una de los tantos componentes del live coding, el resultado es la perfecta unión de las dos: están los ideales del conocimiento compartido y el rechazo de las jerarquías.
La organización toma las herramientas “nerd” que se utilizan como alternativa a la centralización de internet, mientras que las discusiones estéticas rechazan el pensamiento dominante de la academia. “la escena del live coding – escribe Alexandra Cárdenas – es un prototipo de comunidad utópica donde todes pueden contribuir, ya sea que programes hace un día o hace diez años. Es una cuestión política,  que tiene que ver con la integración de la diversidad en comunidades diversas”. En el sitio de la International Conference on Live Coding (ICLC), que aúna los componentes académicos del movimiento, se explicita el rechazo contra cualquier comentario o actitud agresiva en lo relacionado a “género, identidad y expresión de género, orientación sexual, discapacidad, aspecto físico, complexión, raza, religión”. Se necesitaría más tiempo para un análisis correcto de la cultura live coding, contando con todos los testimonios y los rastros, aun en la red, durante estos 15 años, a través de listas de mail, chat, wiki, repositorios y sitios web.


### 2. Les Live Coders son tanto populares como académiques

Gran parte de la comunidad de live coders tiene experiencia en el mundo académico, comúnmente en contextos cercanos a la investigación en nuevas tecnologías musicales. En un cierto sentido, cuanto más elevado es el conocimiento científico del live coder, más ventajas tiene al momento de encontrar nuevas soluciones expresivas, ya hoy en día el live coding aprovecha técnicas dentro de la producción y de las interfaces musicales. En otras palabras, muches livecoders se crean autónomamente los instrumentos y las librerías que necesitan. Esta cercanía al ambiente de la música académica y a la vez con el mundo popular de la música dance es singular para el periodo histórico en el que vivimos, en el cual la música “académica” aún se circunscribe a un pequeño grupo elitista de adeptos al trabajo, de la misma manera que se hacía el siglo pasado. Parece que en el mundo del live coding el gusto popular está encontrando finalmente aspectos comunes de la música más “estudiada”.


### 3. El Live Coding es en vivo

La parte de improvisación del live coding es su característica esencial, una vez más, en contra de la tendencia de la cultura dominante, en la cual las grabaciones en estudio son el principal medio para disfrutar la  música.  Últimamente los grandes servicios de streaming están estimulando un nuevo modo de percibir la pista grabada: ya no vamos a comprar el disco para sumarlo a nuestra colección, ahora encendemos el smartphone y elegimos una canción de un catálogo ampliamente más grande del que encontrábamos en las viejas disquerías. No tenemos la necesidad de filtrar la música que escuchamos en base a una descripción, ni tenemos que limitarnos a escuchar una y otra vez el mismo cd porque es el único que tenemos en casa. Además, ya no nos interesa escuchar siempre la misma música, más bien buscamos cosas nuevas; pierde fuerza el factor que desencadenó muchísimos cambios culturales en las practicas musicales del mil novecientos. La improvisación en el live coding, además de ser perfectamente coherente con la contracultura live coding, podría ser el arma perfecta para insertarse en este nuevo contexto social donde se escucha una canción y se la olvida (casi) instantáneamente.


### 4. El live coding abre nuevos escenarios sin destruir el pasado

Las posibilidades técnicas que ofrece la música por computadora son virtualmente infinitas. Y sin embargo, puede ser por la experiencia de la primer música electrónica (por ejemplo la de Schaeffer y Stockhausen), frente a tantas posibilidades nuevas, que el live coding no rompió totalmente con el lenguaje musical en uso, y elige deliberadamente volver a géneros bien definidos. En realidad, les live coders se mueven por todos los géneros musicales existentes, desde el dance al jazz y al pop, hasta la música más vanguardista. Hasta en este punto es más innovador: es un no-género musical, que no basa su identidad sobre las estructuras semántico-musicales de las que se sirven las  discográficas para sectorizar al mercado, sino más bien sobre los medios de su producción. Un elemento omnipresente del lenguaje en el live coding es la repetición de patrones y a la vez la casualidad de su generación, que recuerda a la música minimalista; esto se debe a los límites de las librerías actuales para la composición algorítmica y no a la práctica del live coding per sé. En general, el live coding intenta innovar sobre lo existente, a veces destruirlo, y no intenta hacerlo mejor con respecto al pasado.


### 5. El live coding es multimedia

Uno de los mejores aspectos del live coding es su interacción multimedio. La actividad musical se conecta con las artes visuales, siempre con código en vivo. Les musiques y les artistas digitales se unen para crear performances en varios niveles, a veces -raramente por cierto- con mensajes explícitos proyectados en la pantalla. En algunas ocasiones les live coders interactúan con actorxs, otres musiques u otres. La performance no se limita a la utilización de la computadora, que se ubica más como una cabina de control que como un instrumento real.


### 6. El live coding no tiene virtualmente límites expresivos

Desde los años "50 hasta ahora, la música por computadora creo una gran cantidad de instrumentos, prácticos y teóricos. Hoy en día sabemos cómo producir sonidos de cualquier tipo desde la computadora, simulando sonidos de objetos reales o inventando nuevos. Sabemos modificar los parámetros de ejecución musical para obtener expresividad y emociones diferentes y sabemos, también, como crear música automáticamente de manera similar al ser humano. Bueno, este último punto es cierto solo en parte, pero es bastante común dejar que la computadora genere  música autónomamente respetando vínculos particulares, como la música que escribe el ser humano. Lo que quiero decir es que la implementación de algoritmos similares en un sistema para el live coding no es impensable, aunque difícil para una comunidad pequeña e hiperfragmentada como la de TOPLAP.


### 7. El live coding es aún un bebé

A pesar de haber nacido hace 15 años y de haber crecido enormemente, TOPLAP es aun pequeña. La tecnología musical crece constantemente pero la comunidad se concentró hasta ahora en las posibles interfaces, experimentando con lenguajes de programación visuales, híbridos y textuales, indagando en particular sobre los lenguajes de programación funcional. Personalmente, por lo poco que he probado, creo que el live coding puede dar un gran salto de calidad implementando otras tecnologías (como las citadas en el punto anterior), que prescinden de la interfaz usada. Los métodos existentes para la música por computadora son adaptables al contexto del live coding ya que no se necesita el desarrollo de una interfaz gráfica completa y la compatibilidad entre componentes complejos como sucede con los software comerciales para producción musical. Un ejemplo de proyecto de investigación que quiere trasladar este género de innovaciones a los contextos de música en tiempo real es MIMIC.

Lo que falta hasta ahora es una gran mente musical que pueda hacer buen uso de la tecnología y que este en grado de presentarla al mundo como un nuevo método capaz de interactuar con otros sujetos expresivos (músiques, actorxs, performers, etc.), sin poner al live coding como una alternativa a los enfoques tradicionales. Sin querer ofender a les live coders actuales, en mi investigación solo he notado un pequeño grupo que reunía varios niveles artísticos, y no a causa de dificultades técnicas – que eventualmente se superaran.

En fin, musiques, ¡acérquense, que acá hay espacio que explorar!

