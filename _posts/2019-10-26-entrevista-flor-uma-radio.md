---
layout: post
categories: posts
title: "Entrevista de radio Flor De Fuega y Uma Chinita de CLiC"
---

# Entrevista de radio Flor De Fuega y Uma Chinita de CLiC

Flor de Fuega es artista plástica, ayudante adscripta de las materias de Lenguaje Multimedial y Fotografía e Imagen Digital, en Bellas Artes de la UNLP. Uma Chinita también de Bellas Artes, trabaja como Vj y escenógrafa. Ambas son parte de CLiC, un colectivo de live coders (programadorxs en vivo)

Entrevista en Radio del Aire FM 89.5 - La Plata - [https://soundcloud.com/radiodelairelaplata](https://soundcloud.com/radiodelairelaplata) - [http://www.radiodelaire.com.ar/](http://www.radiodelaire.com.ar/)

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/701799994&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>
