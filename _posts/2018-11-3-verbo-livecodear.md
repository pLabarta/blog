---
layout: post
categories: posts
title: ":%s/tocar/livecodear/g"
---

# :%s/tocar/livecodear/g *
## Verbo/accion

Nos re-preguntamos varias veces como nombrar lo que hacemos cuando hacemos livecoding. Lo primero que sale es el verbo "tocar":
"hoy tocamos en ..."; que enseguida, nos parece, remite a tocar instrumentos musicales.

Le falta algo al verbo tocar, podriamos decir que "tocamos" las teclas del teclado QWERTY. Pero sin cuestionar eso, sigue sin despegarse
de la tradición musical de tocar instrumentos. Del toque de la técnica, el virtuosismo en los dedos.. que, por suerte, en el livecoding
eso no tiene ningun valor. Podes escribir muy rápido o muy lento y generar sentidos muy similares en "velocidad" (visual y sonora).

Le decian "tocar" a generar un track haciendo mil clicks en un tracker?

Quizas el español se queda chico? Y seguramente si, porque livecoding ya viene del ingles.
Entonces nos dijimos que el verbo "livecodear" puede representar mejor el hecho de programar visuales y sonidos en vivo.

* hoy livecodeamos en tal lugar
* que bella livecodeada eeh
* livecodeemos todis juntxs

[Livecodear](https://livecodear.github.io/) es el nombre, tambien, que tiene la comunidad de livecoding de argentina la cual somos parte.

_* [`:%s/tocar/livecodear/g`](http://vim.wikia.com/wiki/Search_and_replace) es una expresión de [vim](https://www.vim.org/) que remplaza el texto "tocar" por "livecodear" todas
las veces que lo encuentre._
